import React from 'react'
import DisDropDown from './DisDropDown';
// import DisDropDown from './DisDropDown'
import './App.css'; 
import img1 from './Shraddha.jpg';
import img2 from './nora.jpg';
import img3 from './tara.jpeg';

class DropDown extends React.Component {
    constructor(props){
        super(props);
        this.state={value: ' '}

        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }


    handleChange(event)
    {
        this.setState({value:event.target.value});
    }

    handleSubmit(event)
    {
        alert("Your favourite flavour is: "+this.state.value);
        event.prevenDefault();
    }

    // handleChange =event=>{
    //     this.setState({value:event.target.value});
    // }

    render()
    {
        return(
            <div className="App">
            <form onSubmit={this.handleSubmit}>
                <label><h1>Pick Your Favourite Actress</h1>
                    <select value={this.state.value} onChange={this.handleChange}>
                        <option value="">Select Any One</option>
                        <option value={img1}>Shraddha Kapoor</option>
                        <option value={img2}>Nora Fatehi</option>
                        <option value={img3}>Tara Sutaria</option>
                        {/* <option value="mango">Mango</option> */}
                    </select> 
                </label>              
                <DisDropDown fruits={this.state.value}/>
            </form>
            </div>
        );
    }
}

export default DropDown;